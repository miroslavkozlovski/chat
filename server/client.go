package main

import (
	"fmt"
	"log"
	"time"

	"gitlab.com/miroslavkozlovski/chat/protocol"
)

var lastID int

// Client connected client
type Client struct {
	id       int
	name     string
	outputCh chan string
	inputCh  chan string
	logoutCh chan<- *Client
	prt      protocol.Protocol
}

// NewClient return new client
func NewClient(name string, prt protocol.Protocol, logoutCh chan<- *Client) *Client {
	lastID++
	client := &Client{
		id:       lastID,
		name:     name,
		prt:      prt,
		logoutCh: logoutCh,
	}

	// log.Printf("[DEBUG] client %s [%d] connected\n", client.name, client.id)
	return client
}

func (c *Client) read() {
	for {
		line, ok, err := c.prt.ReadMessage()
		if !ok {
			// log.Printf("[DEBUG] client %s [%d] disconnected\n", c.name, c.id)
			c.logoutCh <- c
			return
		}
		if err != nil {
			log.Printf("[ERROR] %v", err)
			time.Sleep(100 * time.Millisecond)
			continue
		}
		c.inputCh <- fmt.Sprintf("%s: %s", c.name, line)
	}
}

func (c *Client) write() {
	for msg := range c.outputCh {
		if err := c.prt.WriteMessage(msg); err != nil {
			log.Printf("[ERROR] %v", err)
		}
	}
}

// Listen handle client reads and writes
func (c *Client) Listen() {
	c.outputCh = make(chan string)
	c.inputCh = make(chan string)

	go c.read()
	go c.write()
}
