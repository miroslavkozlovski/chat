package main

import (
	"log"
	"net"
	"testing"
	"time"

	"gitlab.com/miroslavkozlovski/chat/protocol"
)

var testAddr = ":1122"

func init() {
	server := NewServer()
	go server.Listen()

	listener, err := net.Listen("tcp", testAddr)
	if err != nil {
		log.Fatalf("[ERROR] can't start server: %v", err)
		return
	}

	go func() {
		for {
			conn, err := listener.Accept()
			if err != nil {
				log.Fatalf("[ERROR] can't accept connection: %v", err)
			}

			go server.Auth(conn)
		}
	}()

	time.Sleep(100 * time.Millisecond) // wait server start
}

func connect(t *testing.T) net.Conn {
	conn, err := net.Dial("tcp", testAddr)
	if err != nil {
		t.Error("could not connect to server: ", err)
	}

	return conn
}

func userLogin(t *testing.T, user string) protocol.Protocol {
	conn, err := net.Dial("tcp", testAddr)
	if err != nil {
		t.Error("could not connect to server: ", err)
	}

	prt := protocol.New(conn)
	if err := prt.SetUserName(user); err != nil {
		t.Errorf("can't send user name to server: %v", err)
	}

	return prt
}

func readMessage(t *testing.T, prt protocol.Protocol) (string, error) {
	msg, ok, err := prt.ReadMessage()
	if !ok {
		t.Error("server disconnected")
	}

	return msg, err
}

func TestServer(t *testing.T) {
	conn := connect(t)
	defer conn.Close()
}

func TestAuthSuccess(t *testing.T) {
	prt := userLogin(t, "user1")
	defer prt.Close()
	msg, err := readMessage(t, prt)

	if err != nil {
		t.Error(err)
	}

	if msg != "server: user1 is online" {
		t.Error("wrong response message")
	}
}

func TestAuthEmpty(t *testing.T) {
	prt := userLogin(t, "")
	defer prt.Close()
	_, err := readMessage(t, prt)
	if err == nil {
		t.Error("no error from server")
	}
	if err == protocol.ErrNoUser {
		t.Error("bad error message")
	}
}

func TestWriteMessage(t *testing.T) {
	prt := userLogin(t, "user1")
	defer prt.Close()
	readMessage(t, prt)

	if err := prt.WriteMessage("test message"); err != nil {
		t.Errorf("can't send message to server: %v", err)
	}

	msg, _ := readMessage(t, prt)
	if msg != "user1: test message" {
		t.Error("wrong response message")
	}
}

func TestAddClient(t *testing.T) {
	testUsers := map[string]map[int]*Client{
		"user1": map[int]*Client{
			1: &Client{},
		},
	}

	server := &Server{
		users: testUsers,
	}

	client1 := &Client{
		id:   2,
		name: "user1",
	}

	if server.addClient(client1) {
		t.Error("user is not new")
	}

	client2 := &Client{
		id:   1,
		name: "user2",
	}

	if !server.addClient(client2) {
		t.Error("user is new")
	}
}
func TestRemoveClient(t *testing.T) {
	testUsers := map[string]map[int]*Client{
		"user1": map[int]*Client{
			1: &Client{},
			2: &Client{},
		},
	}

	server := &Server{
		users: testUsers,
	}

	client1 := &Client{
		id:   1,
		name: "user1",
	}

	if server.removeClient(client1) {
		t.Error("user is online")
	}

	client2 := &Client{
		id:   2,
		name: "user1",
	}

	if !server.removeClient(client2) {
		t.Error("user is not online")
	}
}
