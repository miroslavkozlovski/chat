package main

import (
	"flag"
	"log"
	"net"
)

var addr = flag.String("addr", ":8080", "http service address")

func main() {
	flag.Parse()

	server := NewServer()
	go server.Listen()

	listener, err := net.Listen("tcp", *addr)
	if err != nil {
		log.Fatalf("[ERROR] can't start server: %v", err)
	}

	log.Printf("[INFO] starting server on %s", *addr)

	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Printf("[ERROR] can't accept connection: %v", err)
		}

		go server.Auth(conn)
	}
}
