package main

import (
	"fmt"
	"log"
	"net"

	"gitlab.com/miroslavkozlovski/chat/protocol"
)

// Server chat server
type Server struct {
	users       map[string]map[int]*Client
	authCh      chan net.Conn
	loginCh     chan *Client
	logoutCh    chan *Client
	broadcastCh chan string
}

// NewServer return new server
func NewServer() *Server {
	server := &Server{
		users:       make(map[string]map[int]*Client),
		authCh:      make(chan net.Conn),
		loginCh:     make(chan *Client),
		logoutCh:    make(chan *Client),
		broadcastCh: make(chan string),
	}

	return server
}

// Broadcast send message to all clients
func (s *Server) Broadcast(msg string) {
	for _, user := range s.users {
		for _, client := range user {
			client.outputCh <- msg
		}
	}
}

// Login register new client
func (s *Server) Login(client *Client) {
	client.Listen()

	isNew := s.addClient(client)
	go func() {
		if isNew {
			s.broadcastCh <- fmt.Sprintf("server: %s is online", client.name)
		}
		for {
			s.broadcastCh <- <-client.inputCh
		}
	}()
}

// Logout unregister client
func (s *Server) Logout(client *Client) {
	if isEmpty := s.removeClient(client); !isEmpty {
		return
	}

	go func() {
		s.broadcastCh <- fmt.Sprintf("server: %s is offline", client.name)
	}()
}

// Auth authentificate connected user
func (s *Server) Auth(conn net.Conn) {
	prt := protocol.New(conn)
	name, ok, err := prt.GetUserName()
	if !ok {
		// log.Println("[WARN] client disconnected")
		return
	}
	if err != nil {
		log.Printf("[ERROR] %v", err)
		prt.WriteError(err)
		prt.Close()
		return
	}

	s.loginCh <- NewClient(name, prt, s.logoutCh)
}

// Listen handle server events
func (s *Server) Listen() {
	for {
		select {
		case msg := <-s.broadcastCh:
			s.Broadcast(msg)
		case client := <-s.loginCh:
			s.Login(client)
		case client := <-s.logoutCh:
			s.Logout(client)
		}
	}
}

// addClient add client and return true if user is new
func (s *Server) addClient(client *Client) bool {
	user, ok := s.users[client.name]
	if !ok {
		user = make(map[int]*Client)
	}
	user[client.id] = client
	s.users[client.name] = user

	return !ok
}

// removeClient remove client and return true if where is no active user clients
func (s *Server) removeClient(client *Client) bool {
	delete(s.users[client.name], client.id)
	if len(s.users[client.name]) > 0 {
		return false
	}

	delete(s.users, client.name)
	return true
}
