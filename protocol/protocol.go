package protocol

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"strings"
)

var delim = byte('\n')

const (
	messageCode string = "MESSAGE"
	errorCode   string = "ERROR"
	userCode    string = "USER"
)

var (
	// ErrBadCode bad user code error
	ErrBadCode error = errors.New("bad user code")
	// ErrNoUser no user error
	ErrNoUser error = errors.New("no user")
	// ErrEmptyMessage empty message error
	ErrEmptyMessage error = errors.New("message is empty")
)

// Message data transport structure
type Message struct {
	Code string
	Text string
}

// Protocol data transafer protocol interface
type Protocol interface {
	GetUserName() (string, bool, error)
	SetUserName(user string) error
	ReadMessage() (string, bool, error)
	WriteMessage(msg string) error
	WriteError(err error) error
	Close() error
}

type protocol struct {
	rw    bufio.ReadWriter
	rwc   io.ReadWriteCloser
	errCh chan error
}

// New create new data transfer protocol
func New(rwc io.ReadWriteCloser) Protocol {
	rw := bufio.ReadWriter{
		Writer: bufio.NewWriter(rwc),
		Reader: bufio.NewReader(rwc),
	}

	protocol := &protocol{
		rw:  rw,
		rwc: rwc,
	}

	return protocol
}

// Close close connection
func (p *protocol) Close() error {
	return p.rwc.Close()
}

// WriteError send error message to client
func (p *protocol) WriteError(err error) error {
	return p.write(&Message{
		Code: errorCode,
		Text: err.Error(),
	})
}

// WriteMessage send text message to client
func (p *protocol) WriteMessage(msg string) error {
	return p.write(&Message{
		Code: messageCode,
		Text: msg,
	})
}

func (p *protocol) write(msg *Message) error {
	b, err := json.Marshal(msg)
	if err != nil {
		return fmt.Errorf("can't marshal message: %v", err)
	}

	if _, err := p.rw.Write(b); err != nil {
		return fmt.Errorf("can't write message: %v", err)
	}
	if err := p.rw.WriteByte(delim); err != nil {
		return fmt.Errorf("can't write message: %v", err)
	}

	if err := p.rw.Flush(); err != nil {
		return fmt.Errorf("can't flush message: %v", err)
	}

	return nil
}

func (p *protocol) readMessage() (*Message, bool, error) {
	b, err := p.rw.ReadBytes(delim)
	if err == io.EOF {
		return nil, false, nil
	}

	msg := &Message{}
	if err := json.Unmarshal(b, &msg); err != nil {
		return nil, true, fmt.Errorf("can't unmarshal message: %v", err)
	}

	if msg == nil {
		return nil, true, ErrEmptyMessage
	}

	return msg, true, nil
}

// GetUserName get user name from client
func (p *protocol) GetUserName() (string, bool, error) {
	msg, ok, err := p.readMessage()
	if err != nil || !ok {
		return "", ok, err
	}

	if msg.Code != userCode {
		err := ErrBadCode
		return "", ok, err
	}

	name := strings.TrimSpace(msg.Text)

	if name == "" {
		err := ErrNoUser
		return "", ok, err
	}
	return msg.Text, ok, nil
}

// SetUserName set user name in server
func (p *protocol) SetUserName(user string) error {
	if user == "" {
		return ErrNoUser
	}
	return p.write(&Message{
		Code: userCode,
		Text: user,
	})
}

// ReadMessage read message from client
func (p *protocol) ReadMessage() (string, bool, error) {
	msg, ok, err := p.readMessage()
	if err != nil || !ok {
		return "", ok, err
	}

	if msg.Code == errorCode {
		return "", ok, errors.New(msg.Text)
	}

	return msg.Text, ok, nil
}
