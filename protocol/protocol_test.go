package protocol

import (
	"bufio"
	"bytes"
	"errors"
	"io"
	"io/ioutil"
	"strings"
	"testing"
)

type testRWC struct {
	io.ReadWriter
}

func (t *testRWC) Read(p []byte) (int, error) {
	return t.ReadWriter.Read(p)
}

func (t *testRWC) Write(p []byte) (int, error) {
	return t.ReadWriter.Write(p)
}

func (t *testRWC) Close() error {
	return nil
}

func getProtocol(t *testing.T) *protocol {
	var b bytes.Buffer
	rwc := &testRWC{
		ReadWriter: &b,
	}

	rw := bufio.ReadWriter{
		Writer: bufio.NewWriter(rwc),
		Reader: bufio.NewReader(rwc),
	}

	protocol := &protocol{
		rw:  rw,
		rwc: rwc,
	}

	return protocol
}

func isErrDiff(err1, err2 error) bool {
	if err1 == nil && err2 == nil {
		return false
	}

	if err1 != nil && err2 == nil || err1 == nil && err2 != nil {
		return true
	}

	if err1.Error() == err2.Error() {
		return false
	}

	return true
}

func TestWriteMessage(t *testing.T) {
	type option struct {
		inputText  string
		outputText string
		err        error
	}

	options := []option{
		{
			inputText:  "test message",
			outputText: "{\"Code\":\"MESSAGE\",\"Text\":\"test message\"}",
			err:        nil,
		},
		{
			inputText:  "",
			outputText: "{\"Code\":\"MESSAGE\",\"Text\":\"\"}",
			err:        nil,
		},
	}

	testOption := func(inputText string) (string, error) {
		prt := getProtocol(t)
		if err := prt.WriteMessage(inputText); err != nil {
			return "", err
		}

		b, err := ioutil.ReadAll(prt.rw)
		if err != nil {
			return "", err
		}

		s := strings.TrimSpace(string(b))

		return s, nil
	}

	for _, option := range options {
		outputText, err := testOption(option.inputText)
		if isErrDiff(err, option.err) {
			t.Errorf("wrong error message (%v != %v)", err, option.err)
		}

		if outputText != option.outputText {
			t.Errorf("wrong output message (%s != %s)", outputText, option.outputText)
		}
	}
}

func TestReadMessage(t *testing.T) {
	type option struct {
		inputText  string
		outputText string
		err        error
	}

	options := []option{
		{
			inputText:  "{\"Code\":\"MESSAGE\",\"Text\":\"test message\"}",
			outputText: "test message",
			err:        nil,
		},
		{
			inputText:  "{\"Code\":\"ERROR\",\"Text\":\"error message\"}",
			outputText: "",
			err:        errors.New("error message"),
		},
		{
			inputText:  "",
			outputText: "",
			err:        errors.New("can't unmarshal message: unexpected end of JSON input"),
		},
	}

	testOption := func(inputText string) (string, error) {
		prt := getProtocol(t)
		_, err := prt.rw.WriteString(inputText)
		if err != nil {
			return "", err
		}
		if err := prt.rw.WriteByte(byte('\n')); err != nil {
			return "", err
		}
		if err := prt.rw.Flush(); err != nil {
			return "", err
		}

		s, ok, err := prt.ReadMessage()
		if !ok {
			return "", errors.New("can't read from buffer")
		}
		if err != nil {
			return "", err
		}

		return s, nil
	}

	for _, option := range options {
		outputText, err := testOption(option.inputText)
		if isErrDiff(err, option.err) {
			t.Errorf("wrong error message (%v != %v)", err, option.err)
		}

		if outputText != option.outputText {
			t.Errorf("wrong output message (%s != %s)", outputText, option.outputText)
		}
	}
}

func TestSetUser(t *testing.T) {
	type option struct {
		inputText  string
		outputText string
		err        error
	}

	options := []option{
		{
			inputText:  "user1",
			outputText: "{\"Code\":\"USER\",\"Text\":\"user1\"}",
			err:        nil,
		},
		{
			inputText:  "",
			outputText: "",
			err:        ErrNoUser,
		},
	}

	testOption := func(inputText string) (string, error) {
		prt := getProtocol(t)
		if err := prt.SetUserName(inputText); err != nil {
			return "", err
		}

		b, err := ioutil.ReadAll(prt.rw)
		if err != nil {
			return "", err
		}

		s := strings.TrimSpace(string(b))

		return s, nil
	}

	for _, option := range options {
		outputText, err := testOption(option.inputText)
		if isErrDiff(err, option.err) {
			t.Errorf("wrong error message (%v != %v)", err, option.err)
		}

		if outputText != option.outputText {
			t.Errorf("wrong output message (%s != %s)", outputText, option.outputText)
		}
	}
}

func TestGetUser(t *testing.T) {
	type option struct {
		inputText  string
		outputText string
		err        error
	}

	options := []option{
		{
			inputText:  "{\"Code\":\"USER\",\"Text\":\"user1\"}",
			outputText: "user1",
			err:        nil,
		},
		{
			inputText:  "{\"Code\":\"USER\",\"Text\":\"\"}",
			outputText: "",
			err:        ErrNoUser,
		},
		{
			inputText:  "{\"Code\":\"BACODE\",\"Text\":\"user1\"}",
			outputText: "",
			err:        ErrBadCode,
		},
		{
			inputText:  "",
			outputText: "",
			err:        errors.New("can't unmarshal message: unexpected end of JSON input"),
		},
	}

	testOption := func(inputText string) (string, error) {
		prt := getProtocol(t)
		_, err := prt.rw.WriteString(inputText)
		if err != nil {
			return "", err
		}
		if err := prt.rw.WriteByte(byte('\n')); err != nil {
			return "", err
		}
		if err := prt.rw.Flush(); err != nil {
			return "", err
		}

		s, ok, err := prt.GetUserName()
		if !ok {
			return "", errors.New("can't read from buffer")
		}
		if err != nil {
			return "", err
		}

		return s, nil
	}

	for _, option := range options {
		outputText, err := testOption(option.inputText)
		if isErrDiff(err, option.err) {
			t.Errorf("wrong error message (%v != %v)", err, option.err)
		}

		if outputText != option.outputText {
			t.Errorf("wrong output message (%s != %s)", outputText, option.outputText)
		}
	}
}
