package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"time"

	"gitlab.com/miroslavkozlovski/chat/protocol"
)

var addr = flag.String("addr", ":8080", "http service address")

func main() {
	flag.Parse()

	conn, err := net.Dial("tcp", *addr)
	if err != nil {
		log.Fatalf("[ERROR] can't connect to server %s: %v", *addr, err)
	}

	prt := protocol.New(conn)
	if err := login(prt); err != nil {
		log.Fatalf("[ERROR] can't login in chat server: %v", err)
	}
	go read(prt)
	write(prt)
}

func write(prt protocol.Protocol) {
	for {
		var text string
		if _, err := fmt.Scanln(&text); err != nil {
			log.Printf("[ERROR] %v", err)
			continue
		}
		if err := prt.WriteMessage(text); err != nil {
			log.Printf("[ERROR] %v", err)
		}
	}
}

func read(prt protocol.Protocol) {
	for {
		msg, ok, err := prt.ReadMessage()
		if !ok {
			log.Fatalf("[ERROR] server disconnected")
			return
		}
		if err != nil {
			log.Printf("[ERROR] %v", err)
			time.Sleep(100 * time.Millisecond)
			continue
		}
		fmt.Println(msg)
	}
}

func login(prt protocol.Protocol) error {
	user := getUser()
	return prt.SetUserName(user)
}

func getUser() string {
	fmt.Print("enter your name: ")
	var user string
	if _, err := fmt.Scanln(&user); err != nil {
		log.Printf("[ERROR] %v", err)
		return getUser()
	}
	if user == "" {
		return getUser()
	}
	return user
}
